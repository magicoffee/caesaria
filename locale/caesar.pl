{
"##some_food_on_next_month##":"- trochę żywności w nadchodzącym miesiącu"
"##have_no_food_on_next_month##":"- brak żywności w nadchodzącym miesiącu"
"##small_food_on_next_month##":"- bardzo mało żywności w nadchodzącym miesiącu"
"##target_population_is##":"(Populacja docelowa jest"
"##no_target_population##":"(Brak docelowej populacji)"
"##cancel##":"Anuluj"
"##working_build_poor_labor_warning##":"UWAGA: Ograniczony dostęp do pracowników"
"##extm_administration_tlp##":"Struktury administracyjne lub rządowe"
"##mainmenu_package##":"Zaaw. ustawienia"
"##package_options##":"Zaaw. ustawienia"
"##wn_eygptians##":"Egipcjanie"
"##actor_gods_angry##":"Aaa! Bogowie są wściekli! Jesteśmy wszyscy straceni!"
"##ovrm_academy##":"Akademia"
"##academy##":"Akademia"
"##colleges##":"Akademie"
"##current_game_speed_is##":"Aktualna szybkość gry:"
"##save_game_here##":"Zapisz grę do pliku"
"##ovrm_education##":"Wszystko"
"##ovrm_entrertainment##":"Wszystko"
"##amphitheatres##":"Amfiteatry"
"##ovrm_amphitheater##":"Amfiteatry"
"##amphitheater##":"Amfiteatr"
"##barbarian_attack_title##":"Atak barbarzyńców!"
"##waiting_for_free_dock##":"Zakotwiczony, czeka na wolny dok"
"##mainmenu_video##":"Ustawienia wyświetlania"
"##screen_settings##":"Ustawienia wyświetlania"
"##month_4_short##":"Kwi"
"##aqueduct##":"Akwedukt"
"##arabian_stallions##":"Arabskie ogiery"
"##advemployer_panel_workers##":"pracownicy"
"##advemployer_panel_workless##":"Niezatrudnieni pracownicy"
"##advchief_workless##":"Bezrobocie w mieście jest"
"##senatepp_unemployment##":"Bezrobocie"
"##migration_lack_workless##":"Bezrobocie zmniejsza liczbę imigrantów."
"##architect##":"Architekt"
"##mainmenu_dlc_articles##":"Artykuły"
"##clinic##":"Doktor"
"##rioter_in_city_title##":"Zamieszki w mieście"
"##rioter_in_city_text##":"Zamieszki w mieście"
"##month_8_short##":"Sie"
"##ovrm_baths##":"Łaźnie"
"##bath_1##":"Łaźnia"
"##bath##":"Łaźnia"
"##baths##":"Publiczne łaźnie"
"##bathlady##":"Pracownik łaźni"
"##ballista##":"Balista"
"##wn_barbarian##":"Barbarzyńca"
"##ovrm_barber##":"Fryzjer"
"##barber##":"Fryzjer"
"##god_exalted##":"Zachwycony"
"##advchief_employment##":"Zatrudnienie"
"##city_need_workers_title##":"Potrzeba więcej pracowników"
"##library##":"Biblioteka"
"##ovrm_library##":"Biblioteka"
"##wt_librarian##":"Bibliotekarz"
"##libraries##":"Biblioteki"
"##advchief_education##":"Edukacja"
"##education_advisor_title##":"Edukacja"
"##education##":"Edukacja"
"##educationBtnTooltip##":"Edukacja"
"##ovrm_educations##":"Edukacja"
"##britons##":"Brytyjczycy"
"##bridge##":"Most"
"##сaesar##":"Cezar"
"##mainmenu_credits##":"Autorzy"
"##rome_gratitude_request_title##":"Wdzięczność Cesarza."
"##carthago_win_text##":"Dzięki twojemu błyskotliwemu wystąpieniu, zagrożenie ze strony Kartaginy jest nareszcie za nami. Nasi starzy wrogowie są teraz lojalnymi Rzymskimi obywatelami - przynajmniej ci, którzy jeszcze mogą oddychać!"
"##thanks_to##":"PODZIĘKOWANIA DLA:"
"##loading_resources##":"Ładowanie zasobów"
"##engineer_need_workers##":"To miasto było by o wiele lepsze gdyby miało wystarczająco pracowników!"
"##gmenu_file##":"Plik"
"##date_tooltip##":"Data"
"##date##":"Data"
"##scrw_date##":"Data"
"##migration_war_deterring##":"Wojna odstrasza imigrantów!"
"##month_12_short##":"Gru"
"##priest_high_workless##":"Bezrobocie w tym obszarze jest niepokojąco wysokie"
"##100_citizens_in_city##":"Ludność twojego miasta osiągnęła 100 obywateli po raz pierwszy w historii."
"##1000_citizens_in_city##":"Ludność twojego miasta osiągnęła 1000 obywateli po raz pierwszy w historii."
"##10000_citizens_in_city##":"Ludność twojego miasta osiągnęła 10000 obywateli po raz pierwszy w historii."
"##15000_citizens_in_city##":"Ludność twojego miasta osiągnęła 15000 obywateli po raz pierwszy w historii."
"##2000_citizens_in_city##":"Ludność twojego miasta osiągnęła 2000 obywateli po raz pierwszy w historii."
"##20000_citizens_in_city##":"Ludność twojego miasta osiągnęła 20000 obywateli po raz pierwszy w historii."
"##25000_citizens_in_city##":"Ludność twojego miasta osiągnęła 25000 obywateli po raz pierwszy w historii."
"##3000_citizens_in_city##":"Ludność twojego miasta osiągnęła 3000 obywateli po raz pierwszy w historii."
"##500_citizens_in_city##":"Ludność twojego miasta osiągnęła 500 obywateli po raz pierwszy w historii."
"##5000_citizens_inc_city##":"Ludność twojego miasta osiągnęła 5000 obywateli po raz pierwszy w historii."
"##citizens_like_chariot_races##":"Mieszkańcy uwielbiają wyścigi rydwanów."
"##advchief_health_awesome##":"Wskaźnik zdrowia w mieście jest doskonały."
"##rladv_mood##":"Bogowie są"
"##romeGuard_gods_angry##":"Bogowie są źli z tego miasta!"
"##lionTamer_gods_angry##":"Bogowie są tak źli, że wpłynęło to na mojego lwa! Ryczy jak szalony!"
"##barber_gods_angry##":"Bogowie gniewają się. Życzę gubernatorowi aby wybudował więcej światyń!"
"##sentiment_people_love_you##":"Ludzie kochają cię"
"##lgn_wolves##":"Wilki"
"##trouble_need_olive##":"Ten budynek potrzebuje oliwek"
"##building_need_road_access##":"Ten budynek musi być połączony z drogą"
"##fileload_load_tlp##":"Wczytaj ten zapis gry"
"##advemployer_panel_denaries##":"dn"
"##denarii_short##":"dn"
"##barbarian_warrior##":"Barbarzyński wojownik"
"##briton##":"Brytyjczyk"
"##wn_gaul_soldier##":"Galijski wojownik"
"##goth_warrior##":"Gotycki wojownik"
"##hun_warrior##":"Huński wojownik"
"##wn_helveti_soldier##":"Helwecki wojownik"
"##wn_visigoth_soldier##":"Wizygocki wojownik"
"##collapsed_building_title##":"Zapadnięty budynek"
"##debet##":"Przychód"
"##quit##":"Wyjdź"
"##exit_point##":"Punkt wyjścia"
"##earthquake_title##":"Trzęsienie ziemi"
"##ovrm_food##":"Żywność"
"##explosion##":"Eksplozja"
"##enemies_attack_title##":"Wrogowie atakują miasto"
"##infobox_tooltip_exit##":"Zamknij to okno"
"##ovrm_fire##":"Pożar"
"##city_fire_title##":"Pożar w twoim mieście"
"##fishing_boat##":"Łódź rybacka"
"##meat##":"Mięso"
"##continue##":"Kontynuuj"
"##mainmenu_continueplay##":"Kontynuuj"
"##wn_gaul##":"Galowie"
"##ovrm_build##":"Budynek"
"##trouble_need_road_access##":"Ten budynek musi być połączony z drogą"
"##vegetable##":"Warzywa"
"##road_paved_caption##":"Wybrukowana droga"
"##citychart_population##":"Historia"
"##speed_settings##":"Ustawienia szybkości"
"##game_speed_options##":"Ustawienia szybkości"
"##Load_save##":"Wczytaj zapisaną grę"
"##wt_gladiator##":"Gladiator"
"##gladiator_pit##":"Szkoła gladiatorów"
"##gladiatorSchool##":"Szkoła gladiatorów"
"##city_opts_god_on##":"Bogowie: Wł."
"##city_opts_god_off##":"Bogowie: Wył."
"##graphics##":"GRAFIKA:"
"##large##":"duży"
"##big_shack##":"Duża chałupa"
"##statue_big##":"Duża statua"
"##large_temples##":"Duże świątynie"
"##bldm_big_temple##":"Duże światynie"
"##big_villa##":"Duża willa"
"##big_palace##":"Duży pałac"
"##large_temple##":"Duża światynia"
"##big_hut##":"Duży dom"
"##middle_festival##":"Duży festiwal"
"##big_hovel##":"Duża rudera"
"##big_domus##":"Duża insula"
"##big_tent##":"Duży namiot"
"##favor##":"Łaska"
"##senatepp_favour_rating##":"Łaska"
"##wndrt_favour##":"Łaska"
"##emp_open_trade_route##":"Otwórz szlak handlowy"
"##houseBtnTooltip##":"Dom"
"##gmenu_help##":"Pomoc"
"##help##":"Pomoc"
"##mainmenu_dlc_wallpapers##":"Tapety"
"##hippodrome##":"Hipodrom"
"##ovrm_hippodrome##":"Hipodrom"
"##hippodromes##":"Hipodromy"
"##high_fire_risk##":"Wysokie ryzyko pożaru"
"##high_damage_risk##":"Wysokie ryzyko zapadnięcia"
"##charioter_so_hungry##":"Głodny? Mógłbym zjeść konia, przecież tu jest tak mało jedzenia!"
"##teacher_average_life##":"Dałbym temu miastu 8 na 10."
"##scholar_so_hungry##":"Umieram z głodu!"
"##city_need_workers_text##":"Twoje miasto potrzebuje więcej pracowników"
"##wt_immigrant##":"Imigrant"
"##low_crime_risk##":"W tym rejonie występują okazjonalne przestępstwa."
"##no_culture_building_in_city##":"W twoim mieście jest za mało kultury, więc nie masz wskaźnika Kultury."
"##have_less_academy_in_city_0##":"Masz zbyt mało czynnych akademii w twoim mieście. Zbuduj więcej, aby podnieść ten wskaźnik."
"##have_less_library_in_city_0##":"Masz za mało czynnych bibliotek w twoim mieście. Zbuduj więcej, aby podnieść ten wskaźnik."
"##have_less_school_in_city_0##":"Masz zbyt mało czynnych szkół w twoim mieście. Zbuduj więcej, aby podnieść ten wskaźnik."
"##have_less_theater_in_city_0##":"Masz zbyt mało czynnych teatrów w twoim mieście. Zbuduj więcej, aby podnieść ten wskaźnik."
"##have_less_temple_in_city_0##":"Masz za mało miejsc kultu w twoim mieście. Zbuduj więcej, aby podnieść ten wskaźnik."
"##idle_factory_in_city##":"bezczynny przemysł w mieście"
"##idle_factories_in_city##":"beczynny przemysł w mieście"
"##engineer##":"Inżynier"
"##engineering_post##":"Posterunek inżyniera"
"##yes##":"Tak"
"##year##":"Rok"
"##month_1_short##":"Sty"
"##education_awesome##":"Miasto ma wszystkie edukacyjne udogodnienia, które są w idealnym stanie."
"##month_7_short##":"Lip"
"##advemp_emperor_favour##":"Cesarska łaska"
"##mainmenu_loadmap##":"Wczytaj mapę"
"##gmenu_file_restart##":"Restart"
"##save_map##":"Zapisz mapę"
"##no_industries_in_city##":"Brak produkcji w mieście"
"##no_people_in_this_locality##":"Brak ludzi w tym rejonie."
"##no_people_in_city##":"Brak ludzi w mieście!"
"##advchief_employers_ok##":"Miasto nie ma problemów z zatrudnieniem"
"##wn_celt_soldier##":"Celtycki wojownik"
"##children##":"Dziecko"
"##ovrm_colloseum##":"Koloseum"
"##colloseums##":"Koloseum"
"##colloseum##":"Koloseum"
"##balance##":"Bilans"
"##hospital##":"Szpital"
"##ovrm_hospital##":"Szpital"
"##hospitals##":"Szpitale"
"##mars_desc##":"Wojna"
"##senatepp_clt_rating##":"Kultura"
"##wndrt_culture##":"Kultura"
"##initialize_animations##":"Ładowanie animacji"
"##initialize_constructions##":"Ładowanie ustawień konstrukcji"
"##initialize_names##":"Ładowanie nazw obywateli"
"##mainmenu_loadcampaign##":"Wczytaj kampanię"
"##initialize_religion##":"Ładowanie opcji religii"
"##mainmenu_playmission##":"Wczytaj scenariusz"
"##initialize_walkers##":"Ładowanie ustawień przechodniów"
"##load_this_game##":"Wczytaj"
"##start_this_map##":"Wczytaj"
"##gmsndwnd_game_volume##":"Głośność"
"##emigrant_thrown_from_house##":"Zostałem wyrzucony z własnego domu!"
"##god_mars_short##":"Mars"
"##mainmenu_mcmxcviii##":"MCMXCVIII"
"##mercury##":"Merkury"
"##advchief_military##":"Wojsko"
"##adve_military##":"Wojsko"
"##barracks_bad_weapons_bad_workers##":"Ze zredukowaną liczbą personelu i brakiem uzbrojenia, staramy się szkolić nawet najbardziej podstawowych żołnierzy."
"##legion_morale##":"Morale"
"##music##":"MUZYKA:"
"##to_rome_road##":"Do Rzymu"
"##mainmenu_plname##":"Zmień imię"
"##wrath_of_neptune_title##":"Gniew Neptuna"
"##plname_start_new_game##":"Nowa gra"
"##new_map##":"Nowa mapa"
"##mainmenu_newgame##":"Nowa gra"
"##north##":"Północ"
"##northBtnTooltip##":"Północ"
"##wt_homeless##":"Bezdomny"
"##gladiator_pit_no_workers##":"Bez trenerów, ta szkoła nie może szkolić nowych gladiatorów."
"##engineering_post_no_workers##":"Bez inżynierów, ten budynek może się zapaść!"
"##chatioteer_school_no_workers##":"Bez robotników, żaden nowy wózek nie może zostać wyprodukowany. Jeśli hipodrom jest czynny, może ucierpieć w ten skutek."
"##prefecture_no_workers##":"Bez personelu, ten budynek jest czymś więcej niż celem dla wandali."
"##militaryAcademy_no_workers##":"Bez personelu nie możemy poprawić umiejętności nowych żołnierzy. Będą musieli iść prosto do fortu, z nadzieją że będzie lepiej."
"##fort_legionaries_no_workers##":"Bez personelu nie możemy wyszkolić żadnego nowego rekruta. Marsie, pomóż nam w czasach wojny!"
"##forum_no_workers##":"Bez pracowników, ten urząd nic nie znaczy dla skarbca państwa."
"##lion_pit_no_workers##":"Bez treserów, ten dół z lwami nie dostarczy żadnych nowych lwów na zawody."
"##ok##":"OK"
"##gmenu_options##":"Opcje"
"##mainmenu_options##":"Opcje"
"##options##":"Opcje"
"##select_location##":"Wybierz lokację"
"##maximizeBtnTooltip##":"Pokaż panel"
"##minimizeBtnTooltip##":"Ukryj panel"
"##wt_pict_soldier##":"Piktyjski wojownik"
"##wt_prefect##":"Prefekt"
"##prefecture##":"Prefektura"
"##show_prices##":"Pokaż ceny"
"##wt_priest##":"Ksiądz"
"##wt_rioter##":"Buntownik"
"##extm_empire_tlp##":"Mapa Imperium"
"##advchief_religion##":"Religia"
"##ovrm_religion##":"Religia"
"##religion##":"Religia"
"##actor##":"Aktor"
"##close##":"Zamknij"
"##school##":"Szkoła"
"##ovrm_school##":"Szkoła"
"##gmspdwnd_scroll_speed##":"Szybkość przewijania"
"##granery##":"Spichlerz"
"##granary_holds##":"Spichlerz posiada"
"##granaries_holds##":"Spichlerze posiadają"
"##mainmenu_sound##":"Ustawienia dźwięku"
"##sound_settings##":"Ustawienia dźwięku"
"##mainmenu_load##":"Wczytaj grę"
"##mainmenu_loadgame##":"Wczytaj grę"
"##operations_manager##":"Producent gry"
"##gmenu_file_save##":"Zapisz grę"
"##save_city##":"Zapisz grę"
"##gmenu_exit_game##":"Wyjdź z gry"
"##gmenu_file_exit##":"Wyjdź z gry"
"##mainmenu_quit##":"Wyjdź z gry"
"##gmspdwnd_game_speed##":"Szybkość gry"
"##mainmenu_language##":"Język"
"##localization##":"TŁUMACZENIE:"
"##sldr_terrified##":"Przerażony"
"##wt_taxCollector##":"Poborca podatkowy"
"##road_caption##":"Droga"
"##extm_road_tlp##":"Drogi"
"##roadBlock##":"Blokada drogi"
"##south##":"Południe"
"##southEast##":"Południowy wschód"
"##southWest##":"Południowy zachód"
"##day##":"Dzień"
"##days##":"Dni"
"##testers##":"TESTERZY:"
"##tower##":"Wieża"
"##tower_may_build_on_thick_walls##":"Wieże można budować tylko na grubych murach."
"##dn_for_open_trade##":"aby otworzyć szlak handlowy"
"##adve_entertainment##":"Rozrywka"
"##entertainment_advisor_title##":"Rozrywka"
"##ovrm_aborigen##":"Aborygen"
"##venus##":"Wenus"
"##exit##":"Wyjście"
"##exit_without_saving_question##":"Wyjść bez zapisywania?"
"##advchief_haveprofit##":"W tym roku majątek wzrósł o"
"##advchief_havedeficit##":"W tym roku majątek zmalał o"
"##governor_palace_2##":"Willa gubernatora"
"##governorVilla##":"Willa gubernatora"
"##fullscreen_on##":"Pełny ekran"
"##RomeChastenerArmy_troops_at_our_gates##":"Cesarskie wojska u naszych bram"
"##destroy_bridge_warning##":"Niszcz mosty z ostrożnością. Wyizolowane społeczeństwo wkrótce wyginie jeśli zostanie odcięte od drogi do Rzymu."
"##enter_your_name##":"Wpisz swoje imię"
"##warning_some##":"Ostrzeżenia - Wył"
"##warning_full##":"Ostrzeżenia - Wł"
"##warning##":"Ostrzeżenia"
"##city_warnings_on##":"Ostrzeżenia: Wł."
"##city_warnings_off##":"Ostrzeżenia: Wył"
"##ovrm_water##":"Woda"
"##water_caption##":"Woda"
"##water_supply##":"Woda"
"##wine##":"Wino"
"##vinard##":"Farma winogron"
"##plname_continue##":"Kontynuuj"
"##wrath_of_ceres_title##":"Gniew Ceres"
"##tower_no_workers##":"Bez pracowników, nie możemy obsadzić naszej balisty i zatrudnić strażników do patrolowania murów."
"##wrath_of_mars_title##":"Gniew Marsa"
"##wrath_of_mercury_title##":"Gniew Merkurego"
"##wrath_of_venus_title##":"Gniew Wenus"
"##doctor_gods_angry##":"Jeśli nie okażemy bogom więcej szacunku, poczujemy ich gniew na sobie!"
"##shipyard##":"Stocznia"
"##west##":"Zachód"
"##wn_visigoth##":"Wizygoci"
"##mainmenu_dlc_main##":"Jak zbudować Rzym"
"##extm_housing_tlp##":"Domy"
"##wt_wolf##":"Wilk"
"##emperor_anger##":"Gniew Cesarza"
"##infobox_tooltip_help##":"Pokaż pomoc dla tego okienka"
"##time_since_last_gift##":"Czas od ostatniego podarunku"
"##destroyed_building_title##":"Zniszczony budynek"
"##well##":"Studnia"
"##city_zoom_on##":"Powiększenie: Wł."
"##city_zoom_off##":"Powiększenie: Wył."
"##gmenu_file_mainmenu##":"Wyjdź do menu"
"##to_empire_road##":"Do Imperium"
"##goto##":"Idź do"
"##goto_empire_map##":"Mapa Imperium"
"##mission_wnd_tocity##":"Do miasta"
"##wt_engineer##":"Inżynier"
"##savedlg_continue##":"Zapisz"
}